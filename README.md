# RUSHin
Analyzes acute accent marked text in Russian in an attempt to find rules (or suggestive guidelines) governing word stress.

## Database Dictionary
### words
Contains records of the different words analyzed by the program. Each word is only included in the table once.

| Field                   | Type     | Key     | Reference | Definition |
| :---------------------: | :------: | :-----: | :-------: | :--------: |
| word               | TEXT     | PRIMARY | -         | the actual string of the word |
| character           | TEXT     | -       | -         | single character string of the stressed vowel in the word          |
| char_index     | INTEGER  | -       | -         | 1-indexed positon of the stressed character in the word among the characters in the word          |
| syll_index | INTEGER  | -       | -         | 1-indexed positon of the stressed syllable in the word among the syllables in the word          |
| vowel_number   | INTEGER  | -       | -         | 1-indexed positon of the stressed vowel in the word among the vowels in the word          |

### syllables
Contains records of the different syllables analyzed by the program. Each syllable is only included in the table once. Syllables with and without stressed vowels are considered different.

| Field                   | Type     | Key     | Reference | Definition |
| :---------------------: | :------: | :-----: | :-------: | :--------: |
| syll           | TEXT     | PRIMARY | -         | the actual string of the syllable |
| character           | TEXT     | -       | -         | single character string of the stressed vowel in the syllable          |
| char_index     | INTEGER  | -       | -         | 1-indexed positon of the stressed character in the syllable among the characters in the syllable          |

### contains
Relational table used to map words and syllables together.

| Field     | Type     | Key              | Reference                | Definition |
| :-------: | :------: | :--------------: | :----------------------: | :--------: |
| word      | TEXT     | PRIMARY, FOREIGN | words(word)         | the word which contains the syllable              |
| syll  | TEXT     | PRIMARY, FOREIGN | syllables(syll) | the syllable which is contained by the word           |
