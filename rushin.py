#!/usr/bin/env python3

import sys
import getopt
import sqlite3
from sqlite3 import Error
import db_manage

ACCENT = u'\u0301'  # Accute accent mark, denotes stress
ALPHABET = ["б", "в", "г", "д", "ж", "з", "к", "л", "м", "н", "п", "р",
                 "с", "т", "ф", "х", "ц", "ч", "ш", "щ", "й", "а", "э", "ы",
                 "у", "о", "я", "е", "ё", "ю", "и", "ь", "ъ", "Б", "В", "Г",
                 "Д", "Ж", "З", "К", "Л", "М", "Н", "П", "Р", "С", "Т", "Ф",
                 "Х", "Ц", "Ч", "Ш", "Щ", "Й", "А", "Э", "Ы", "У", "О", "Я",
                 "Е", "Ё", "Ю", "И", "Ь", "Ъ"]
VOWELS = ["а", "э", "ы", "у", "о", "я", "е", "ё", "ю", "и", "А", "Э", "Ы",
              "У", "О", "Я", "Е", "Ё", "Ю", "И"]
CONSONANTS = [c for c in ALPHABET if c not in VOWELS]
SONORANTS = ["й", "р", "л", "м", "н", "Й", "Р", "Л", "М", "Н"]

all_words = dict()
all_syllables = dict()


def strip_text(text, include_stress=True, valid=ALPHABET):
    """Trims a string of unwanted characters.

    Removes every character from a string which is not a valid russian letter.

    Args:
    text (string): The text from which to remove characters
    include_stress (bool): If true, include accent marks
    valid (list): acceptable characters to include in the otput

    Returns:
    string: Lowercase string with all non-letters or stress marks removed
    """
    return "".join(filter(lambda x:
        (x in valid) or (x.isspace()) or
        (include_stress and x == ACCENT), text)).lower()


# Though the loop appears to be zero-indexed, it actually returns the position
# of the accent mark — which is always 1 more than the character which is
# stressed (as unicode stressed characters are actually represented "и´" as
# opposed to "и́".) This gives the correct 1-indexed position of the rendered
# ("и́") plaintext.
def find_stress_char(str):
    u"""Identifies the stressed character in a string.

    Iterates through a string until a stressed character is found. A
    stressed character is one with an accute accent mark over it (и́).

    Args:
    str (string): The single string in which to find the stress

    Returns:
    string: Single character string of the stressed letter, none if no
    stress mark
    int: 1-indexed position of the stressed character, none if no stress mark
    """
    i = 0
    for character in str:
        if character == ACCENT:
            return (previous_char + character), i  # b/c "и́">>"и´"
        previous_char = character
        i += 1
    return None, None


def find_stress_syllable(split_word, stressed_char):
    u"""Identifies the syllable containing a stressed russian letter.

    Iterates through a list consisting of the syllables of a single russian
    word to find a syllable containing a specified stressed letter.
    A stressed letter is one with an accute accent mark over it (и́).

    Args:
    split_word (list): The list of syllables constituting a single russain word
    stressed_char (string): A string of the syllable containing the stress mark

    Returns:
    string: syllable containing the stressed character, none if no stress mark
    int: 1-indexed position of the stressed syllable, none if no stress mark

    Restrictions:
    Can only be called on a list of syllables not on an entire word (even if
    the word is only one syllable), i.e. after split_syllables has been called.
    """
    for syllable in split_word:
        if stressed_char and stressed_char in syllable:
            return syllable, split_word.index(syllable)+1
    return None, None


def count_symbols(input_string, symbol_list, stop_string=None,
                  inclusive=True, bad_stop=False):
    """Counts the occurances of any in specified set of characters in a string.

    Counts the number of characters from a specified list present in a
    particular string. Can count the number of such characters in the entire
    string or up-to or including the first occurnce of a specified substring.

    Args:
    input_string (string): The string to search for characters in.
    symbol_list (list): List of 1-character stings to count in input_string.
    stop_string (string): (Default=None) Substring of which find the position
    in input_string.
    inclusive (boolean): (Default=True) If true, then characters of stop_string
    will be included in search.

    Returns:
    int: number of characters in the examined range of input_string which are
    listed in symbol_list

    Restrictions:
    While stop_sting may be a multi-character string, Symbol_list only supports
    single characters. In other words, this cannot count multi-character
    strings proceeding stop_char, only single characters. (i.e. Number of
    vowels, not number of groups of vowels.)
    """
    the_count = 0
    to_return = ""
    if stop_string:
        if inclusive:
            for character in input_string[:input_string.find(stop_string)
                                          +len(stop_string)]:
                if character in symbol_list:
                    to_return += character
                    the_count += 1
                elif bad_stop:
                    return to_return, the_count
        else:
            for character in input_string[:input_string.find(stop_string)]:
                if character in symbol_list:
                    to_return += character
                    the_count += 1
                elif bad_stop:
                    return to_return, the_count
    else:
        for character in input_string:
            if character in symbol_list:
                to_return += character
                the_count += 1
            elif bad_stop:
                return to_return, the_count
    return to_return, the_count


# The first large for-loop splits the string into a list after every vowel,
# except for the last vowel. The second for-loop iterates through the items
# in the list inital_split and modifies them as follows:
# A syllable in inital_split is added to final_split; characters will be
# appended to the end of this syllable. A sonorant consonants, and any
# non-sonorant consonants preceding them are appeneded to the previous
# syllable, as long as that sonorant character does not directly precede a
# vowel. (In other words, as long as that sonorant is not the last character
# in a group of 1 or more consonants preceeding a vowel).
def split_syllables(word):
    """Separates a russian word into distinct syllables.

    Splits a string, consisting of a single russian word, into a list of
    syllables as defined by Avanesov's Syllabification Rule, aka the
    "Acsendent Sonority" Rule, which details the criteria for the proper
    syllibification of Russian words.

    Args:
    word (string): The string to split into syllables.

    Returns:
    list: List of syllables of word
    int: Number of syllables in word
    """
    to_join = []
    initial_split = []
    possible_join = []
    final_split = []

    for char in word:
        # Concatenate accent to end of last syllable added
        if char == ACCENT:
            initial_split[-1] += char
        # Join consonants into syllable until vowel
        elif char not in VOWELS:
            to_join.append(char)
        # Join vowel to syllable end
        # Add group of consonants and vowel to list of syllables
        else:
            to_join.append(char)
            initial_split.append("".join(to_join))
            del to_join[:]
    # words with no vowels
    if not initial_split:
        return "".join(to_join), 1
    # Append consonants which appear after final vowel to end of last syllable
    if to_join:
        initial_split[-1] += "".join(to_join)
        del to_join[:]

    skip_char = False
    final_split.append(initial_split[0])
    for item in initial_split[1:]:
        for y in range(len(item)):
            # Skip when 2 character long sonorant added in previous iteration
            if skip_char:
                skip_char = False
            # Vowels will never be appended to the preceeding syllable
            elif (item[y] in VOWELS):
                break
            # Non-sonorant consonants appended to previous syllable
            elif (item[y] not in SONORANTS):
                possible_join.append(item[y])  # Add depending on sonorant
            # Only append sonorant consonant if next letter is not a vowel
            elif (item[y+1] not in VOWELS):
                possible_join.append(item[y])
                # Check for 2 character long sonorants
                if ((item[y] != "й") and
                    ((item[y+1] == "ь") or (item[y+1] == item[y]))):
                    # Only append 2 chars if next letter is not a vowel
                    if item[y+2] not in VOWELS:
                        possible_join.append(item[y+1])
                        to_join = to_join + possible_join
                        skip_char = True
                else:
                    to_join = to_join + possible_join
                del possible_join[:]
        # Append consonants to previous syllable
        final_split[-1] += "".join(to_join)
        # Append remainder of syllable (not added to prev syllable) to list
        final_split.append(item[len("".join(to_join)):])
        del to_join[:]
        del possible_join[:]
    return final_split, len(final_split)


def new_word(text, syllables):
    """Prepares dictionary arg for db_manage->db_insert_row into word table

    Creates a dictionary which contains values for a new record in the words
    table. The key for each dictionary item is the field name in the table.
    The value is the new value to be inserted into the table.

    Args:
    text (string): The russian word to be added to the database
    syllables (list): The list of the syllables of the russian word

    Returns:
    dictionary: The dictionary containing the values for a new record
    """
    d = {}
    d["word"] = text
    d["character"], d["char_index"] = find_stress_char(text)
    d["syll_index"] = find_stress_syllable(syllables, d["character"])[1]
    d["vowel_number"] = find_stress_char(strip_text(text, True, VOWELS))[1]
    return d


def new_syllable(text):
    """Prepares dictionary arg for db_manage->db_insert_row into syllable table

    Creates a dictionary which contains values for a new record in the syllable
    table. The key for each dictionary item is the field name in the table.
    The value is the new value to be inserted into the table.

    Args:
    text (string): The russian syllable to be added to the database

    Returns:
    dictionary: The dictionary containing the values for a new record
    """
    d = {}
    d["syll"] = text
    d["character"], d["char_index"] = find_stress_char(text)
    return d


def new_contains(word_text, syllable_text):
    """Prepares dictionary arg for db_manage->db_insert_row into contains table

    Creates a dictionary which has values for a new record in the contains
    table. The key for each dictionary item is the field name in the table.
    The value is the new value to be inserted into the table.

    Args:
    word_text (string): The russian word to be added to the database
    syllable_text (string): The russian syllable to be added to the database

    Returns:
    dictionary: The dictionary containing the values for a new record
    """
    d = {}
    d["word"] = word_text
    d["syll"] = syllable_text
    return d


def main(argv):
    input_file = ''
    output_file = ''

    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["infile=", "outfile="])
    except getopt.GetoptError:
        print('RUSHin.py -i <inputfile> -o <outputfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('RUSHin.py -i <inputfile> -o <outputfile>')
            sys.exit()
        elif opt in ("-i", "--infile"):
            input_file = arg
        elif opt in ("-o", "--outfile"):
            output_file = arg

    connection = db_manage.db_init()
    with open(input_file, encoding='utf-8') as input_file:
        input_text = input_file.read()
    for word in strip_text(input_text).split():
        syll_list = split_syllables(word)[0]
        word_dict = new_word(word, syll_list)
        if word_dict["character"]:
            db_manage.db_insert_row(connection, "words", word_dict)
            for syllable in syll_list:
                syll_dict = new_syllable(syllable)
                db_manage.db_insert_row(connection, "syllables", syll_dict)
                contains_dict = new_contains(word_dict["word"],
                                             syll_dict["syll"])
                db_manage.db_insert_row(connection, "contains", contains_dict)

    connection.close()



            # consonant groups before and after stress vowel
            # consonant groups before and after unstressed vowel
            # letter directly before and behind stressed vowel
            # letter directly before and behind unstressed vowel
            # syllable directly before and after stressed syllable
            # all syllables before and after stressed syllable


if __name__ == "__main__":
    main(sys.argv[1:])
