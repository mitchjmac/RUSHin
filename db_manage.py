#!/usr/bin/env python3

import sqlite3
from sqlite3 import Error

db_file = "russian_db.sqlite"

# Used to create new tables in the database.
# Used in db_init as arguments for db_exec.
words_table = """CREATE TABLE IF NOT EXISTS words (
    word TEXT PRIMARY KEY,
    character TEXT,
    char_index INTEGER,
    syll_index INTEGER,
    vowel_number INTEGER
);"""
syllable_table = """CREATE TABLE IF NOT EXISTS syllables (
    syll TEXT PRIMARY KEY,
    character TEXT,
    char_index INTEGER
);"""
contains_table = """CREATE TABLE IF NOT EXISTS contains (
    word TEXT,
    syll TEXT,
    PRIMARY KEY (word, syll),
    FOREIGN KEY (word) REFERENCES words(word),
    FOREIGN KEY (syll) REFERENCES syllables(syll)
);"""


def db_insert_row(connection, table, column_dict):
    """Inserts a new record into a table in a database.

    Replaces portions of a string with the table name, the column names,
    and "?"s equal to the number of columns for use with sqlite's execute.
    Takes the modified string and the list of values to insert into the table
    and uses sqlite's execute to insert a new row into the table.

    Args:
    connection (sqlite connection): the database containing the table
    table (string): the name of the table to insert into
    column_dict (dictionary): keys are column names, values are field values

    Returns:
    None
    """
    query = "INSERT OR IGNORE INTO {0} ({1}) VALUES ({2});"
    columns = ','.join(column_dict.keys())
    placeholders = ','.join(["?"] * len(column_dict))
    cur = connection.cursor()
    cur.execute(query.format(table, columns, placeholders),
                            list(column_dict.values()))
    connection.commit()


def db_exec(connection, command_string):
    """Executes an sqlite command and performs exception checking.

    Uses sqlite execute to execute the command passed in to the function.
    On exception, it rollsback and closes the database.

    Args:
    connection (sqlite connection): the database containing the table
    command_string (string): the sqlite string to execute

    Returns:
    None
    """
    try:
        cur = connection.cursor()
        cur.execute(command_string)
    except Error as e:
        connection.rollback()
        connection.close()
        raise(e)


def db_init():
    """Creates a new sqlite database connection and add table to it.

    Creates a new database connection (creating the database if it
    doesn't exits). After which, this function adds the three tables to
    the database if they don't exist.

    Args:
    connection (sqlite connection): the database containing the table
    command_string (string): the sqlite string to execute

    Returns:
    None
    """
    try:
        connect = sqlite3.connect(db_file)
        connect.execute('pragma foreign_keys=ON')
    except Error as e:
        connect.close()
        raise(e)
    db_exec(connect, words_table)
    db_exec(connect, syllable_table)
    db_exec(connect, contains_table)
    connect.commit()
    return connect
